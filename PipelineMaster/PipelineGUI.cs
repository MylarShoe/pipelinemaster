﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Net;

namespace PipelineMaster
{
	public partial class PipelineGUI : Form
	{
		public PipelineGUI()
		{
			InitializeComponent();
			StartUp();
			Duration.Validating += new CancelEventHandler(Duration_Validating);
			StartTimeCode.Validating += new CancelEventHandler(StartTimeCode_Validating);
		}

		private void StartUp()
		{
			StartTimeCode.Text = DateTime.Now.ToString("HH:mm:ss");
			StartTimeCode.Text = StartTimeCode.Text.Insert(StartTimeCode.Text.Length, ":00");
			StartTimeCode.Select(11, 11);
			Duration.Text = "00:00:00:00";
			Duration.Select(11, 11);
			StartTimeCode.Enabled = false;
			Duration.Enabled = false;
		}

		public string connectTest(string URL, string Port)
		{
			string sURL;
			sURL = "http://" + URL + ":" + Port + "/record/status";

			WebRequest wrGETURL;
			wrGETURL = WebRequest.Create(sURL);
			string returnValue = "";

			//WebProxy myProxy = new WebProxy("myproxy", 80);
			//myProxy.BypassProxyOnLocal = true;

			//wrGETURL.Proxy = WebProxy.GetDefaultProxy();
			try
			{
				Stream objStream;
				objStream = wrGETURL.GetResponse().GetResponseStream();

				StreamReader objReader = new StreamReader(objStream);

				string sLine = "";
				int i = 0;

				while (sLine != null)
				{
					i++;
					sLine = objReader.ReadLine();
					if (sLine != null)
					{
						Console.WriteLine("{0}:{1}", i, sLine);
						returnValue = sLine;
					}
				}
				//return "Connected";
			}
			catch
			{
				Console.WriteLine("No Connection");
				return "Unavailable";
			}
			if (returnValue.Contains("Waiting"))
				return "Connected";
			else if (returnValue.Contains("Opened"))
				return "Recording";
			else if (returnValue.Contains("Closed"))
				return "Connected";
			else
				return "Error";
		}

		private void Duration_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			string temp = Duration.Text.ToString();
			int templen = temp.Length;
			string input = null;
			for (int i = 0; i < templen; i++)
			{
				if (temp[i] == ':')
					continue;
				else
					input += temp[i];
			}
			int dur_length = input.Length;
			if (dur_length > 8)
			{
				Duration.Text = "00:00:00:00";
				return;
			}
			char[] pos;
			pos = new char[8];
			for (int i = 0; i < pos.Length; i++)
			{
				pos[i] = '0';
			}
			for (int i = 0; i < dur_length; i++)
			{
				int sub = 1 + i;
				int spot = 7 - i;

				if (input[input.Length - sub] != ':')
				{
					pos[spot] = input[input.Length - sub];
				}
			}
			int result = 0;
			string hour = pos[0].ToString() + pos[1].ToString();
			result = hour.CompareTo("09");
			if (result > 0)
				hour = "08";
			string minute = pos[2].ToString() + pos[3].ToString();
			result = minute.CompareTo("59");
			if (result > 0)
				minute = "59";
			string second = pos[4].ToString() + pos[5].ToString();
			result = second.CompareTo("59");
			if (result > 0)
				second = "59";
			string frame = pos[6].ToString() + pos[7].ToString();
			result = frame.CompareTo("29");
			if (result > 0)
				frame = "29";
			string output = hour + ":" + minute + ":" + second + ":" + frame;
			Duration.Text = output;
		}

		private void StartTimeCode_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			string temp = StartTimeCode.Text.ToString();
			int templen = temp.Length;
			string input = null;
			for (int i = 0; i < templen; i++)
			{
				if (temp[i] == ':')
					continue;
				else
					input += temp[i];
			}
			int dur_length = input.Length;
			if (dur_length > 8)
			{
				Duration.Text = "00:00:00:00";
				return;
			}
			char[] pos;
			pos = new char[8];
			for (int i = 0; i < pos.Length; i++)
			{
				pos[i] = '0';
			}
			for (int i = 0; i < dur_length; i++)
			{
				int sub = 1 + i;
				int spot = 7 - i;

				if (input[input.Length - sub] != ':')
				{
					pos[spot] = input[input.Length - sub];
				}
			}
			int result = 0;
			string hour = pos[0].ToString() + pos[1].ToString();
			result = hour.CompareTo("23");
			if (result > 0)
				hour = "23";
			string minute = pos[2].ToString() + pos[3].ToString();
			result = minute.CompareTo("59");
			if (result > 0)
				minute = "59";
			string second = pos[4].ToString() + pos[5].ToString();
			result = second.CompareTo("59");
			if (result > 0)
				second = "59";
			string frame = pos[6].ToString() + pos[7].ToString();
			result = frame.CompareTo("29");
			if (result > 0)
				frame = "29";
			string output = hour + ":" + minute + ":" + second + ":" + frame;
			StartTimeCode.Text = output;
		}

		public bool IsValidIp(string addr)
		{
			IPAddress ip;
			bool valid = !string.IsNullOrEmpty(addr) && IPAddress.TryParse(addr, out ip);
			return valid;
		}

		private void AddButton_Click(object sender, EventArgs e)
		{
			if (Address.Text == "")
				return;
			if (ServerPort.Text == "")
				return;
			if (!IsValidIp(Address.Text))
			{
				MessageBox.Show("Invalid IP Address", "Error",MessageBoxButtons.OK , MessageBoxIcon.Error);
				return;
			}
			ListViewItem lvi = new ListViewItem(Address.Text);
			lvi.SubItems.Add(ServerPort.Text);
			lvi.SubItems.Add("Connecting...");
			ServerList.Items.Add(lvi);
			string temp = connectTest(Address.Text, ServerPort.Text);
			ServerList.Items[ServerList.Items.Count - 1].SubItems[2].Text = temp;
		}

		private void RemoveServer_Click(object sender, EventArgs e)
		{
			foreach (ListViewItem eachItem in ServerList.SelectedItems)
			{
				ServerList.Items.Remove(eachItem);
			}
		}

		private void UseTC_CheckedChanged(object sender, EventArgs e)
		{
			if (UseTC.Checked == true)
				StartTimeCode.Enabled = true;
			if (UseTC.Checked == false)
				StartTimeCode.Enabled = false;
		}

		private void SetDuration_CheckedChanged(object sender, EventArgs e)
		{
			if (SetDuration.Checked == true)
				Duration.Enabled = true;
			if (SetDuration.Checked == false)
				Duration.Enabled = false;
		}

		public string submitURL(string sURL)
		{
			WebRequest wrGETURL;
			wrGETURL = WebRequest.Create(sURL);

			try
			{
				Stream objStream;
				objStream = wrGETURL.GetResponse().GetResponseStream();

				StreamReader objReader = new StreamReader(objStream);

				string sLine = "";
				int i = 0;

				while (sLine != null)
				{
					i++;
					sLine = objReader.ReadLine();
					if (sLine != null)
						Console.WriteLine("{0}:{1}", i, sLine);
				}
				Console.ReadLine();
				return "Success";
			}
			catch
			{
				Console.WriteLine("No Connection");
				return "Failure";
			}
		}

		private void StartTrigger_Click(object sender, EventArgs e)
		{
			string[] commands;
			commands = new string[ServerList.Items.Count];

			for (int i = 0; i < ServerList.Items.Count; i++)
			{
				if (ServerList.Items[i].SubItems[2].Text != "Connected")
				{
					commands[i] = "";
					continue;
				}
				String CommandString = "http://" + ServerList.Items[i].SubItems[0].Text + ":" + ServerList.Items[i].SubItems[1].Text + "/record/start";

				if (UseTC.Checked == true)
				{
					CommandString += "?start=" + StartTimeCode.Text;
				}
				if (SetDuration.Checked == true && UseTC.Checked == false)
				{
					CommandString += "?duration=" + Duration.Text;
				}
				if (SetDuration.Checked == true && UseTC.Checked == true)
				{
					CommandString += "&duration=" + Duration.Text;
				}
				commands[i] = CommandString;
				//Console.WriteLine(CommandString);
			}
			for (int i = 0; i < commands.Length; i++)
			{
				if (commands[i] != "")
					submitURL(commands[i]);
			}
			for (int i = 0; i < commands.Length; i++)
			{
				if (commands[i] == "")
				{
					string temp = "Unable to connect to Server\nhttp://" + ServerList.Items[i].SubItems[0].Text + ":" + ServerList.Items[i].SubItems[1].Text;
					MessageBox.Show(temp, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
				}
			}
			UpdateAllServerStatus();
		}

		private void StopTrigger_Click(object sender, EventArgs e)
		{
			string[] commands;
			commands = new string[ServerList.Items.Count];

			for (int i = 0; i < ServerList.Items.Count; i++)
			{
				if (ServerList.Items[i].SubItems[2].Text != "Connected")
				{
					commands[i] = "";
					continue;
				}
				String CommandString = "http://" + ServerList.Items[i].SubItems[0].Text + ":" + ServerList.Items[i].SubItems[1].Text + "/record/stop";
				commands[i] = CommandString;
			}
			for (int i = 0; i < commands.Length; i++)
			{
				if (commands[i] != "")
					submitURL(commands[i]);
			}
			UpdateAllServerStatus();
		}

		private void UpdateAllServerStatus()
		{
			for (int i = 0; i < ServerList.Items.Count; i++)
			{
				string status = connectTest(ServerList.Items[i].SubItems[0].Text, ServerList.Items[i].SubItems[1].Text);
				ServerList.Items[i].SubItems[2].Text = status;
			}
		}

		private void UpdateTigger_Click(object sender, EventArgs e)
		{
			UpdateAllServerStatus();
		}
	}
}
