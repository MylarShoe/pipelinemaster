﻿namespace PipelineMaster
{
	partial class PipelineGUI
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PipelineGUI));
			this.StartTimeCode = new System.Windows.Forms.TextBox();
			this.Duration = new System.Windows.Forms.TextBox();
			this.AddServer = new System.Windows.Forms.GroupBox();
			this.AddButton = new System.Windows.Forms.Button();
			this.ServerPort = new System.Windows.Forms.TextBox();
			this.Address = new System.Windows.Forms.TextBox();
			this.ServerList = new System.Windows.Forms.ListView();
			this.ServerAddress = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
			this.Port = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
			this.Status = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
			this.Remove = new System.Windows.Forms.Button();
			this.StartTrigger = new System.Windows.Forms.Button();
			this.RemoveServer = new System.Windows.Forms.GroupBox();
			this.UseTC = new System.Windows.Forms.CheckBox();
			this.SetDuration = new System.Windows.Forms.CheckBox();
			this.statusStrip1 = new System.Windows.Forms.StatusStrip();
			this.SubmissionStatus = new System.Windows.Forms.ToolStripStatusLabel();
			this.StopTrigger = new System.Windows.Forms.Button();
			this.ServerControls = new System.Windows.Forms.GroupBox();
			this.UpdateTigger = new System.Windows.Forms.Button();
			this.AddServer.SuspendLayout();
			this.RemoveServer.SuspendLayout();
			this.statusStrip1.SuspendLayout();
			this.ServerControls.SuspendLayout();
			this.SuspendLayout();
			// 
			// StartTimeCode
			// 
			this.StartTimeCode.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.06283F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.StartTimeCode.Location = new System.Drawing.Point(901, 47);
			this.StartTimeCode.Name = "StartTimeCode";
			this.StartTimeCode.Size = new System.Drawing.Size(264, 44);
			this.StartTimeCode.TabIndex = 0;
			this.StartTimeCode.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// Duration
			// 
			this.Duration.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.06283F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.Duration.Location = new System.Drawing.Point(901, 111);
			this.Duration.Name = "Duration";
			this.Duration.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Duration.ShortcutsEnabled = false;
			this.Duration.Size = new System.Drawing.Size(264, 44);
			this.Duration.TabIndex = 0;
			this.Duration.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// AddServer
			// 
			this.AddServer.Controls.Add(this.Address);
			this.AddServer.Controls.Add(this.ServerPort);
			this.AddServer.Controls.Add(this.AddButton);
			this.AddServer.Location = new System.Drawing.Point(58, 506);
			this.AddServer.Name = "AddServer";
			this.AddServer.Size = new System.Drawing.Size(851, 121);
			this.AddServer.TabIndex = 5;
			this.AddServer.TabStop = false;
			this.AddServer.Text = "Add Server";
			// 
			// AddButton
			// 
			this.AddButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.047121F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.AddButton.Location = new System.Drawing.Point(692, 43);
			this.AddButton.Name = "AddButton";
			this.AddButton.Size = new System.Drawing.Size(128, 44);
			this.AddButton.TabIndex = 2;
			this.AddButton.Text = "Add";
			this.AddButton.UseVisualStyleBackColor = true;
			this.AddButton.Click += new System.EventHandler(this.AddButton_Click);
			// 
			// ServerPort
			// 
			this.ServerPort.AcceptsReturn = true;
			this.ServerPort.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.06283F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.ServerPort.Location = new System.Drawing.Point(492, 43);
			this.ServerPort.Name = "ServerPort";
			this.ServerPort.Size = new System.Drawing.Size(179, 44);
			this.ServerPort.TabIndex = 1;
			// 
			// Address
			// 
			this.Address.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.06283F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.Address.Location = new System.Drawing.Point(28, 43);
			this.Address.Name = "Address";
			this.Address.Size = new System.Drawing.Size(445, 44);
			this.Address.TabIndex = 0;
			// 
			// ServerList
			// 
			this.ServerList.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.ServerAddress,
            this.Port,
            this.Status});
			this.ServerList.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.06283F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.ServerList.FullRowSelect = true;
			this.ServerList.GridLines = true;
			this.ServerList.Location = new System.Drawing.Point(58, 207);
			this.ServerList.Name = "ServerList";
			this.ServerList.Size = new System.Drawing.Size(1107, 282);
			this.ServerList.TabIndex = 6;
			this.ServerList.UseCompatibleStateImageBehavior = false;
			this.ServerList.View = System.Windows.Forms.View.Details;
			// 
			// ServerAddress
			// 
			this.ServerAddress.Text = "Server Address";
			this.ServerAddress.Width = 231;
			// 
			// Port
			// 
			this.Port.Text = "Port";
			this.Port.Width = 71;
			// 
			// Status
			// 
			this.Status.Text = "Status";
			this.Status.Width = 250;
			// 
			// Remove
			// 
			this.Remove.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.047121F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.Remove.Location = new System.Drawing.Point(35, 43);
			this.Remove.Name = "Remove";
			this.Remove.Size = new System.Drawing.Size(163, 44);
			this.Remove.TabIndex = 7;
			this.Remove.Text = "Remove";
			this.Remove.UseVisualStyleBackColor = true;
			this.Remove.Click += new System.EventHandler(this.RemoveServer_Click);
			// 
			// StartTrigger
			// 
			this.StartTrigger.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.93194F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.StartTrigger.Location = new System.Drawing.Point(50, 42);
			this.StartTrigger.Name = "StartTrigger";
			this.StartTrigger.Size = new System.Drawing.Size(125, 50);
			this.StartTrigger.TabIndex = 8;
			this.StartTrigger.Text = "Start";
			this.StartTrigger.UseVisualStyleBackColor = true;
			this.StartTrigger.Click += new System.EventHandler(this.StartTrigger_Click);
			// 
			// RemoveServer
			// 
			this.RemoveServer.Controls.Add(this.Remove);
			this.RemoveServer.Location = new System.Drawing.Point(931, 506);
			this.RemoveServer.Name = "RemoveServer";
			this.RemoveServer.Size = new System.Drawing.Size(234, 121);
			this.RemoveServer.TabIndex = 9;
			this.RemoveServer.TabStop = false;
			this.RemoveServer.Text = "Remove Server";
			// 
			// UseTC
			// 
			this.UseTC.AutoSize = true;
			this.UseTC.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.06283F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.UseTC.Location = new System.Drawing.Point(534, 49);
			this.UseTC.Name = "UseTC";
			this.UseTC.Size = new System.Drawing.Size(338, 42);
			this.UseTC.TabIndex = 10;
			this.UseTC.Text = "Start from Timecode";
			this.UseTC.UseVisualStyleBackColor = true;
			this.UseTC.CheckedChanged += new System.EventHandler(this.UseTC_CheckedChanged);
			// 
			// SetDuration
			// 
			this.SetDuration.AutoSize = true;
			this.SetDuration.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.06283F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.SetDuration.Location = new System.Drawing.Point(533, 111);
			this.SetDuration.Name = "SetDuration";
			this.SetDuration.Size = new System.Drawing.Size(339, 42);
			this.SetDuration.TabIndex = 11;
			this.SetDuration.Text = "Set Record Duration";
			this.SetDuration.UseVisualStyleBackColor = true;
			this.SetDuration.CheckedChanged += new System.EventHandler(this.SetDuration_CheckedChanged);
			// 
			// statusStrip1
			// 
			this.statusStrip1.ImageScalingSize = new System.Drawing.Size(32, 32);
			this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.SubmissionStatus});
			this.statusStrip1.Location = new System.Drawing.Point(0, 767);
			this.statusStrip1.Name = "statusStrip1";
			this.statusStrip1.Size = new System.Drawing.Size(1225, 40);
			this.statusStrip1.TabIndex = 12;
			this.statusStrip1.Text = "statusStrip1";
			// 
			// SubmissionStatus
			// 
			this.SubmissionStatus.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
			this.SubmissionStatus.Name = "SubmissionStatus";
			this.SubmissionStatus.Size = new System.Drawing.Size(100, 35);
			this.SubmissionStatus.Text = "Waiting";
			// 
			// StopTrigger
			// 
			this.StopTrigger.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.93194F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.StopTrigger.Location = new System.Drawing.Point(216, 42);
			this.StopTrigger.Name = "StopTrigger";
			this.StopTrigger.Size = new System.Drawing.Size(125, 50);
			this.StopTrigger.TabIndex = 13;
			this.StopTrigger.Text = "Stop";
			this.StopTrigger.UseVisualStyleBackColor = true;
			this.StopTrigger.Click += new System.EventHandler(this.StopTrigger_Click);
			// 
			// ServerControls
			// 
			this.ServerControls.Controls.Add(this.StartTrigger);
			this.ServerControls.Controls.Add(this.StopTrigger);
			this.ServerControls.Controls.Add(this.UpdateTigger);
			this.ServerControls.Location = new System.Drawing.Point(580, 633);
			this.ServerControls.Name = "ServerControls";
			this.ServerControls.Size = new System.Drawing.Size(585, 115);
			this.ServerControls.TabIndex = 14;
			this.ServerControls.TabStop = false;
			this.ServerControls.Text = "Server Controls";
			// 
			// UpdateTigger
			// 
			this.UpdateTigger.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.93194F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.UpdateTigger.Location = new System.Drawing.Point(386, 42);
			this.UpdateTigger.Name = "UpdateTigger";
			this.UpdateTigger.Size = new System.Drawing.Size(163, 50);
			this.UpdateTigger.TabIndex = 14;
			this.UpdateTigger.Text = "Update";
			this.UpdateTigger.UseVisualStyleBackColor = true;
			this.UpdateTigger.Click += new System.EventHandler(this.UpdateTigger_Click);
			// 
			// PipelineGUI
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 25F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.SystemColors.Control;
			this.ClientSize = new System.Drawing.Size(1225, 807);
			this.Controls.Add(this.UseTC);
			this.Controls.Add(this.StartTimeCode);
			this.Controls.Add(this.SetDuration);
			this.Controls.Add(this.Duration);
			this.Controls.Add(this.ServerList);
			this.Controls.Add(this.ServerControls);
			this.Controls.Add(this.statusStrip1);
			this.Controls.Add(this.RemoveServer);
			this.Controls.Add(this.AddServer);
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.MaximizeBox = false;
			this.MaximumSize = new System.Drawing.Size(1251, 878);
			this.MinimumSize = new System.Drawing.Size(1251, 878);
			this.Name = "PipelineGUI";
			this.Text = "Pipeline Master";
			this.AddServer.ResumeLayout(false);
			this.AddServer.PerformLayout();
			this.RemoveServer.ResumeLayout(false);
			this.statusStrip1.ResumeLayout(false);
			this.statusStrip1.PerformLayout();
			this.ServerControls.ResumeLayout(false);
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.TextBox StartTimeCode;
		private System.Windows.Forms.TextBox Duration;
		private System.Windows.Forms.GroupBox AddServer;
		private System.Windows.Forms.Button AddButton;
		private System.Windows.Forms.TextBox ServerPort;
		private System.Windows.Forms.TextBox Address;
		private System.Windows.Forms.ListView ServerList;
		private System.Windows.Forms.ColumnHeader ServerAddress;
		private System.Windows.Forms.ColumnHeader Port;
		private System.Windows.Forms.ColumnHeader Status;
		private System.Windows.Forms.Button Remove;
		private System.Windows.Forms.Button StartTrigger;
		private System.Windows.Forms.GroupBox RemoveServer;
		private System.Windows.Forms.CheckBox UseTC;
		private System.Windows.Forms.CheckBox SetDuration;
		private System.Windows.Forms.StatusStrip statusStrip1;
		private System.Windows.Forms.ToolStripStatusLabel SubmissionStatus;
		private System.Windows.Forms.Button StopTrigger;
		private System.Windows.Forms.GroupBox ServerControls;
		private System.Windows.Forms.Button UpdateTigger;

	}
}

